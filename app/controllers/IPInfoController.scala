package controllers

import javax.inject.{Inject, Singleton}
import akka.actor.ActorSystem
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import services.IPInfoService

import scala.concurrent.ExecutionContext

@Singleton
class IPInfoController @Inject()(cc: ControllerComponents,
                                 actorSystem: ActorSystem,
                                 ipInfoService: IPInfoService
                                )(implicit exec: ExecutionContext) extends AbstractController(cc) {

  /** Creates an Action that returns the `IPInfo` mapped to the ip via IPInfoDB API */
  def ipInfo = Action.async { request =>
    ipInfoService
      .getIPInfo(request.remoteAddress)
        .map { ipInfo => Ok(Json.toJson(ipInfo)) }
  }
}