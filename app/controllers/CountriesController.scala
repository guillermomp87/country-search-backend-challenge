package controllers

import akka.actor.ActorSystem
import javax.inject.{Inject, Singleton}
import models.{Country, StringHelper}
import play.api.Environment
import play.api.{Configuration, Logger}
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, ControllerComponents}

import services.IPInfoService

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CountriesController @Inject()(cc: ControllerComponents,
                                    actorSystem: ActorSystem,
                                    ws: WSClient,
                                    ipInfoService: IPInfoService,
                                    configuration: Configuration,
                                    environment: Environment
                                      )
                                   (implicit exec: ExecutionContext,
                                    assetsFinder: AssetsFinder) extends AbstractController(cc) {

  private lazy val countries = Country.getCountries(environment)

  /** Returns country search HTML page */
  def countrySearch = Action {
    Ok(views.html.countries("Countries"))
  }

  /** Returns a list of countries as json.
    * - Filter: If a name filter is provided the list will be filtered by countries that start with the given name.
    * - Sorting: Returned list is sorted by distance to callers country (extracted from IP) */
  def getCountries(nameFilter: Option[String]) = Action.async { request =>
    Logger.info(s"query: $nameFilter")
    (for {
      countries <- getCountriesF(nameFilter)
      ip <- ipInfoService.getIPInfo(request.remoteAddress)
    } yield {
      countries.sortBy { country => country.location.distance(ip.location) }
    }).map { countries => Ok(Json.toJson(countries)) }
  }


  /** Returns a list of countries.
    * - Filter: If a name filter is provided the list will be filtered by countries that start with the given name. */
  def getCountriesF(nameFilter: Option[String]): Future[Seq[Country]] = {
    Future {
      nameFilter.map(StringHelper.cleanupToCompare) match {
        case Some(filter) if filter.nonEmpty => countries.filter { country => country.comparableName.startsWith(filter) }
        case _ => countries
      }
    }
  }
}
