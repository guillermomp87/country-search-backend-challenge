package models

import play.api.libs.json._

/** Info extracted from the ip address*/
case class IPInfo(countryName: String, location: Location)

object IPInfo {

  implicit val writes = new Writes[IPInfo] {
    override def writes(ipInfo: IPInfo): JsValue = Json.obj(
      "countryName" -> ipInfo.countryName,
      "lat" -> ipInfo.location.lat,
      "lng" -> ipInfo.location.lng
    )
  }
}
