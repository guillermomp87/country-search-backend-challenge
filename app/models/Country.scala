package models

import play.api.Environment
import play.api.libs.json._
import play.api.libs.functional.syntax._

/** A country with its name and location, plus a cleaned up version of the name for simpler direct comparision */
case class Country(name: String, comparableName: String, location: Location)

object Country {

  implicit val writes = new Writes[Country] {
    override def writes(country: Country): JsValue = Json.obj(
      "name" -> country.name
    )
  }

  /** Returns the list of countries loaded from a file or empty seq if not found */
  def getCountries(environment: Environment): Seq[Country] = {

    // Reads for the "countries.json" format
    implicit val reads: Reads[Country] = (
      (JsPath \ "name").read[String] and
        (JsPath \ "latlng")(0).read[Double] and
        (JsPath \ "latlng")(1).read[Double]
      )((name, lat, lng) => Country(name, StringHelper.cleanupToCompare(name), Location(lat, lng)))

    environment.resourceAsStream("countries.json")
      .map(Json.parse)
      .flatMap(json => json.asOpt[Seq[Country]])
      .getOrElse(Seq.empty)
  }
}