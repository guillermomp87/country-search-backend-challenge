package models

case class Location(lat: Double, lng: Double) {

  /** Distance in meters to a locations */
  def distance(to: Location): Int = {
    Location.distance(this, to)
  }
}

object Location {

  private val AVERAGE_RADIUS_OF_EARTH_KM = 6371

  /** Distance in meters between two locations */
  def distance(from: Location, to: Location): Int = {
    val latDistance = Math.toRadians(from.lat - to.lat)
    val lngDistance = Math.toRadians(from.lng - to.lng)
    val sinLat = Math.sin(latDistance / 2)
    val sinLng = Math.sin(lngDistance / 2)
    val a = sinLat * sinLat +
      (Math.cos(Math.toRadians(from.lat)) *
        Math.cos(Math.toRadians(to.lat)) *
        sinLng * sinLng)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    (AVERAGE_RADIUS_OF_EARTH_KM * 1000 * c).toInt
  }
}
