package models

import java.text.Normalizer.Form
import java.text.Normalizer

object StringHelper {

  /** Cleans up diacritics and makes lower case to make strings directly comparable */
  def cleanupToCompare(text: String): String = removeDiacritics(text).toLowerCase

  /** Removes all diacritics from a string */
  def removeDiacritics(text: String): String = {
    Normalizer.normalize(text, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
  }
}
