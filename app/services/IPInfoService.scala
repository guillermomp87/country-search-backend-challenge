package services

import javax.inject.{Inject, Singleton}
import models.{IPInfo, Location}
import play.api.libs.ws.WSClient
import play.api.Configuration

import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

trait IPInfoService {
  def getIPInfo(remoteAddress: String): Future[IPInfo]
}

/** IPInfoService implementation using IPStackService as primary service but adding IPInfoDBService and IPApiService
  * as fallbacks */
@Singleton
class CombinedIpApiService @Inject()(wSClient: WSClient, configuration: Configuration)(implicit executionContext: ExecutionContext) extends IPInfoService {

  /** Primary IPInfoService to use */
  private val ipStackService = new IPStackService(wSClient, configuration)
  /** 1st fallback option */
  private val ipInfoDBService = new IPInfoDBService(wSClient, configuration)
  /** 2nd fallback option */
  private val ipApiService = new IPApiService(wSClient, configuration)

  def getIPInfo(remoteAddress: String): Future[IPInfo] = {
    ipStackService.getIPInfo(remoteAddress)
      .fallbackTo(ipInfoDBService.getIPInfo(remoteAddress))
      .fallbackTo(ipApiService.getIPInfo(remoteAddress))
  }
}

/** IPInfoService implementation for http://ip-api.com */
class IPApiService @Inject()(wSClient: WSClient, configuration: Configuration)(implicit executionContext: ExecutionContext) extends IPInfoService {

  private val baseURL: String = "http://ip-api.com"

  /** Custom IPInfo deserializer for ip-api */
  implicit val reads: Reads[IPInfo] = (
    (JsPath \ "country").read[String] and
      (JsPath \ "lat").read[Double] and
      (JsPath \ "lon").read[Double]
    )((name, lat, lng) => IPInfo(name, Location(lat, lng)))

  def getIPInfo(remoteAddress: String): Future[IPInfo] = {
    wSClient
      .url(s"$baseURL/json") // Note: Append /$remoteAdress for ip lookup
      .get()
      .map { response =>
        response.json.as[IPInfo]
      }
  }
}

/** IPInfoService implementation for http://api.ipstack.com */
class IPStackService @Inject()(wSClient: WSClient, configuration: Configuration)(implicit executionContext: ExecutionContext) extends IPInfoService {

  private val baseURL: String = "http://api.ipstack.com"
  private val apiKey: String = configuration.get[String]("ipstack.token")

  /** Custom IPInfo deserializer for ipstack */
  implicit val reads: Reads[IPInfo] = (
    (JsPath \ "country_name").read[String] and
      (JsPath \ "latitude").read[Double] and
      (JsPath \ "longitude").read[Double]
    )((name, lat, lng) => IPInfo(name, Location(lat, lng)))

  def getIPInfo(remoteAddress: String): Future[IPInfo] = {
    wSClient
      .url(s"$baseURL/check")
      .addQueryStringParameters("access_key" -> apiKey)
      .get()
      .map { response =>
        response.json.as[IPInfo]
      }
  }
}
/** IPInfoService implementation for https://api.ipinfodb.com */
class IPInfoDBService @Inject()(wSClient: WSClient, configuration: Configuration)(implicit executionContext: ExecutionContext) extends IPInfoService {

  private val baseURL: String = "https://api.ipinfodb.com"
  private val apiKey: String = configuration.get[String]("ipinfodb.token")

  /** Custom IPInfo deserializer for ipinfodb
    * Note: latitude & longitude come as json strings */
  implicit val reads: Reads[IPInfo] = (
    (JsPath \ "countryName").read[String] and
      (JsPath \ "latitude").read[String] and
      (JsPath \ "longitude").read[String]
    ) { (name, lat, lng) => IPInfo(name, Location(lat.toDouble, lng.toDouble)) }

  def getIPInfo(remoteAddress: String): Future[IPInfo] = {
    wSClient
      .url(s"$baseURL/v3/ip-city/")
      .addQueryStringParameters(
        "key" -> apiKey,
        //"ip" -> remoteAddress, // Note: Using server remote address as replacement for client local address.
        "format" -> "json")
      .get()
      .map { response =>
        response.json.as[IPInfo]
      }
  }
}
