$( function() {

    // Cache of autocomplete queries
    var cache = {};

    // Bind to autocomplete
    var elem = $( "#autocomplete" ).autocomplete({
        source: function (request, response) {
            var term = request.term;
            // 1- Use cache if available
            if ( term in cache ) {
              response( cache[ term ] );
              return;
            }
            // 2- If cache not available query autocomplete
            $.getJSON("/api/v1.0/countries?nameFilter=" + encodeURIComponent(request.term), function (data) {
                // Map to jquery UI model for autocompletion, which consists of json with label and value fields.
                // For now we can use the country name for both. Value could be useful for passing country code.
                var countries = $.map(data, function (item, index) {
                    return {
                        label: item.name,
                        value: item.name
                    };
                })
                cache[ term ] = countries;
                response( countries );
            });
        },
        minLength: 1, // Default 1
        delay: 300 // Default 300
    });

    // Highlight: https://stackoverflow.com/questions/9887032/how-to-highlight-input-words-in-autocomplete-jquery-ui
    elem.data("ui-autocomplete")._renderItem = function (ul, item) {
            // A) Using indexes provided by the backend
            // B) Regex approach (currently not matching backend)
            // var newText = String(item.value).replace(new RegExp(this.term, "gi"), "<b>$&</b>");
            // C) Simple 0 to length replacement that matches backend implementation
            var highlightedText = item.label.substring(0, this.term.length)
            var remainderText = item.label.substring(this.term.length)
            var newText = "<b>" + highlightedText + "</b>" + remainderText
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<div>" + newText + "</div>")
                .appendTo(ul);
        };
} );